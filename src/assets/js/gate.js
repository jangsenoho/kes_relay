

//게이트 페이지 hover 이벤트
window.onload = function (){
var LeftGate = document.querySelector(".gate-page .gates .prefix");
var RightGate = document.querySelector(".gate-page .gates .suffix");
var GateBg =  document.querySelector(".gate-page");
// var SwitchBtn = document.querySelector('.pc .prefix .swith-off');

LeftGate.addEventListener('mouseover', function(){
  LeftGate.parentNode.setAttribute('class','gates left-gate-hover');
  GateBg.style.backgroundImage ="url('../assets/images/img-leftselect-bg.png')";
});
LeftGate.addEventListener('mouseout', function(){
  LeftGate.parentNode.classList.remove('left-gate-hover');
  GateBg.style.backgroundImage ="url('../assets/images/gate-bg.png')";
});

RightGate.addEventListener('mouseover', function(){
  RightGate.parentNode.setAttribute('class','gates right-gate-hover');
  GateBg.style.backgroundImage ="url('../assets/images/img-rightselect-bg.png')";
});
RightGate.addEventListener('mouseout', function(){
  RightGate.parentNode.classList.remove('right-gate-hover');
  GateBg.style.backgroundImage ="url('../assets/images/gate-bg.png')";
});

$('.gate-page .gates .suffix').hover(
  function () {
      var self = this;
      hovertimer = setTimeout(function(){
          $(self).addClass('hover');
      }, 350);
  },
  function () {
      clearTimeout(hovertimer);
      $('.gate-page .gates .suffix').removeClass('hover');
  }
);
$('.gate-page .gates .prefix').hover(
  function () {
      var self = this;
      hovertimer = setTimeout(function(){
          $(self).addClass('hover');
      }, 350);
  },
  function () {
      clearTimeout(hovertimer);
      $('.gate-page .gates .prefix').removeClass('hover');
  }
);
}