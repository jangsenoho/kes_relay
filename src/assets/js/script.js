//HEADER, FOOTER 불러오기
$(document).ready( function() {
   $("#header").load("header.html"); 
   $("#footer").load("footer.html");   
  //$("#floatWrap").load("float-menu.html");   

  //floating active
  var floatDim = document.getElementById('floatDim');
  var floating = document.getElementById('floatToggle');

  var floatSearch = document.getElementById('floatSearch');
  var floatVirtual = document.getElementById('floatVirtual');
  var floatAgora = document.getElementById('floatAgora');
  var floatTop = document.getElementById('floatTop');

  floating.addEventListener('click', function(){
    this.classList.toggle('active');
    floatDim.classList.toggle('active');
    floatSearch.classList.toggle('active');
    floatVirtual.classList.toggle('active');
    floatAgora.classList.toggle('active');
    floatTop.classList.toggle('active');
  });

  //2022 04 08 input 포커스 이벤트 추가 
  var icon = "<i class='icon-xbtnc'></i>";
  $(document).on('focus', 'input', function () {
    $(".input-close-icon").append(icon)
  })

  $(document).on('blur', 'input', function () {
    $(this).closest(".input-close-icon").find('.icon-xbtnc').remove();
  })
});
  

$(document).ready(function () {
  var TopMenu, TopMenuPosition;
  TopMenu = document.querySelector('.float-menu');
  TopMenuPosition = TopMenu.offsetTop;

  function submenu_bar_fixed() {
    if (window.pageYOffset >= TopMenuPosition) {
      TopMenu.classList.add("show");
    } else {
      TopMenu.classList.remove("show");
    }
  }
  document.addEventListener('scroll', submenu_bar_fixed);
})




//탭배너
$(document).ready(function(){
	
	$('.tab-banner .item').click(function(){
		var tab_id = $(this).attr('data-tab');

		$(this).siblings('.tab-banner .item').removeClass('current');
		$(this).closest('.tab-container').find('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})
});


//datepicker
$("input[id^='datepicker']").each(function () {
  var _this = this.id;
  $('#' + _this).datepicker({
    regional: 'ko',
    changeMonth: true,
    changeYear: true,
    dateFormat: 'yy-mm-dd',
    showOn: "button",
    buttonImage: "../assets/images/ico-calendar.png",
    buttonImageOnly: true,
  });
});


//모달창
function modalClose() {
  $('.modal .close-btn').click(function() {
    $(this).parents('.modal-wrap').removeClass('is-visible');
    $('body').css("overflow", "auto");
  });
}

function modalOpen(target) {
  $('.' + target).addClass('is-visible');
  $('body').css("overflow", "hidden");
}


//아코디언 
$(function() {

  var group = $(".accordion");
  group.each(function() {
    var _group = new GroupBox(this);
  });
  function GroupBox(groupElement) {

    var box = $(groupElement).find(".box");
    var title = $(groupElement).find(".box .accordion-title a");

    box.each(function(idx) {
      var newBox = new RootBox(this);
      if (idx > 0) {
        newBox.siblingsClose();
      }
    });
  }
  function RootBox(boxElement) {
    var _this = this;
    var boxEl = $(boxElement);
    var target = $(boxEl).find(".accordion-title a");
    var cont = $(boxEl).find(".panel");

    target.on("click", anchorClickEvent);
    function anchorClickEvent() {

      if (cont.is(':hidden')) {
        _this.open();
      } else {
        _this.close();
      }
    }
    _this.siblingsClose = function() {
      cont.css('display', 'none');
    };
    _this.open = function() {
      cont.css('display', 'flex')
      target.addClass('open');
      target.removeClass('close')
    };
    _this.close = function() {
      cont.css('display', 'none')
      target.addClass('close');
      target.removeClass('open')
    }
  }
});

//모바일 햄버거 매뉴 
function OpenMenu() {
  document.querySelector('.header').classList.add('gnb-open');
}
function CloseMenu() {
  document.querySelector('.header').classList.remove('gnb-open')
}









//메인페이지 fullpage 스크립트 - 수정필요 - ys
$(document).ready(function () {
  
  
  var winHWidth = $(window).width();
  var winHeight = $(window).height();
  var secHeight = $(".section").height();  
  
  // window.addEventListener("wheel", function(e){
  //   e.preventDefault();
  // },{passive : false});

  var $html = $("html"); 
  var page = 1;      
  var lastPage = $(".section").length; 
  var scrollId = '';  
  var cntPage = 0;
  $html.animate({scrollTop:0},5);


  $(window).on("wheel", function(e){  
    
      if($html.is(":animated")) return;

      if(e.originalEvent.deltaY > 0){
        if(page== lastPage) return;
        page++;
      }else if(e.originalEvent.deltaY < 0){
        if(page == 1) return;
        page--;
      }  
      cntPage = page - 1;
      scrollId = '#section' + cntPage;

      var posTop = (page-1) * secHeight;

      var offTop = $(scrollId).offset().top;

      $html.animate({scrollTop : offTop});    
  });

});


//북마크 ☆ -ys
$(document).ready(function(){	
	$('.ico-bookmark').click(function(){
    $(this).toggleClass('active');
	})
});


